<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CT_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("CT_COMPONENT_DESC"),
	"ICON" => "/images/icon.gif",
	"SORT" => 20,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "project",
			"NAME" => GetMessage("CT_COMPONENT_PATH"),
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "create.pdf.eventslist",
			),
		),
	),
);
?>
<?php
use VP\Entities\Client;
use VP\Entities\Company;
use VP\Entities\Hazard;
use VP\Entities\RiskLevel;
use VP\Entities\SectionCompany;
use VP\Entities\WorkPlace;
use VP\Entities\WorkZone;
use VP\Entities\CalculationIpr;
use VP\ORM\BitrixEntity\File;
use VP\ORM\Db\Manager;
use WS\Tools\Module;

class CreatePdfEventsList extends CBitrixComponent {

    private $userId;
    private $id;
    private $type;
    private $companyId;
    private $workPlaces;
    private $riskLevels;

    /** @var  Client */
    private $client;

    /** @var Company*/
    private $company;

    public function executeComponent() {
        /**
         * @global CUser $USER
         * @global CMain $APPLICATION
         * @global CDatabase $DB
         */
        global $USER, $APPLICATION, $DB;
        $this->setFrameMode(true);

        $request = $this->getRequest();
        $this->id = $request->get('id');
        $this->type = $request->get('type');
        $this->companyId = $request->get('companyId');

        if (!$this->companyId || !$this->type) {
            return;
        }
        $this->userId = $USER->GetID();
        if (!$this->userId) {
            return;
        }
        if (!$this->initClient()) {
            return;
        }

        foreach ($this->client->lastPdf as $file) {
            if (!empty($file->id)) {
                CFile::Delete($file->id);
            }
        }

        $this->initRiskLevel();

        $result['success'] = false;
        $fileLink = "";
        if ($this->type == 'list-events') {
            $fileLink = $this->createEventsListPdfFile();
        }

        if (!empty($fileLink)) {
            $name = 'Перечень мероприятий';
            $link = '<a href="'.$fileLink.'" download class="vp-link download-link">'.$name.'</a>';
            $result = array(
                'success' => true,
                'link' => $link,
                'name' => 'Перечень мероприятий'
            );
        }

        $APPLICATION->RestartBuffer();
        echo json_encode($result);
        die();
    }

    /**
     * @throws Exception
     */
    private function createEventsListPdfFile() {
        $documentRoot = $_SERVER['DOCUMENT_ROOT'];
        $html = '';
        $htmlHead = file_get_contents(__DIR__. "/html/list_events/pdfHead.html");
        $result = $this->prepareHtmlHead($htmlHead);
        $html.=$result;
        $result = $this->createHtmlBody();
        $html.=$result;
        $htmlFooter = file_get_contents(__DIR__. "/html/list_events/pdfFooter.html");
        $result = $this->prepareHtmlFooter($htmlFooter);
        $html.=$result;

        if (!empty($this->company->memberCommission)) {
            $result = $this->createHtmlCommission();
            $html.=$result;
        }

        $result = $this->createEndHtml();
        $html.=$result;

        $tmpFile = $documentRoot . "/upload/tmp/eventslist_" . $this->client->id . "_" . $this->companyId . "_" . date('H_i_s') . ".html";
        file_put_contents($tmpFile, $html);

        $tmpPdfFile = $documentRoot . "/upload/tmp/tmp_eventslist_pdf.pdf";

        $command = "/usr/local/bin/wkhtmltopdf --lowquality --encoding 'utf-8' --orientation 'Landscape' " . $tmpFile . " " . $tmpPdfFile . "";
        exec($command);

        $createPdfFile = $documentRoot . "/upload/tmp/Перечень мероприятий.pdf";
        $pdfContent = file_get_contents($tmpPdfFile);
        file_put_contents($createPdfFile, $pdfContent);

        $file = $this->makeFile($createPdfFile);
        unlink($tmpFile);
        unlink($tmpPdfFile);
        unlink($createPdfFile);

        $this->savePdfFileToEntity($file);

        return $file->getSrc();
    }

    /**
     * @throws Exception
     */
    private function prepareHtmlHead($html) {
        $this->company = $this->getOrmManager()->getById(Company::className(), $this->companyId);
        $html = strtr($html, array(
            '#COMPANY_NAME#' => $this->company->fullName['TEXT'],
        ));
        return $html;
    }

    private function prepareHtmlFooter($html) {
        $html = strtr($html, array(
            '#POSITION_CHAIRMAN#' => $this->company->positionChairmanCommission,
            '#CHAIRMAN#' => $this->company->chairmanCommission
        ));
        return $html;
    }

    /**
     * @throws Exception
     */
    private function createHtmlCommission() {
        /** @var array $members */
        $members = $this->company->memberCommission;
        $html = file_get_contents(__DIR__. "/html/list_events/pdfHeadCommission.html");
        foreach ($members as $member) {
            $item = explode(';', $member);
            $arItem['POSITION'] = $item[0];
            $arItem['NAME'] = $item[1];
            $itemMember = $this->getMemberItem($arItem);
            $html.=$itemMember;
        }
        $html.='</table>';
        return $html;
    }

    /**
     * @throws Exception
     */
    private function createHtmlBody() {
        global $html;
        $this->workPlaces = $this->getWorkPlaces();
        $sections = $this->getSections();
        $html = '';

        $this->recursiveMake($sections, $html);

        return $html;
    }

    private function recursiveMake(&$arSections, $html) {
        global $html;
        foreach ($arSections as $key => &$arSection) {
            $itemSection = $this->getSectionItem($arSection['name']);
            $html.=$itemSection;
            if (count($this->workPlaces[$arSection['id']]) > 0) {
                foreach ($this->workPlaces[$arSection['id']] as $workplace) {
                    $itemWorkplace = $this->getWorkplaceItem($workplace);
                    $html.=$itemWorkplace;
                }
            }
            if (count($arSection["child"]) > 0) {
                $this->recursiveMake($arSection["child"], $html);
            }
        }
    }

    private function createEndHtml() {
        return '</body></html>';
    }

    private function getRequest() {
        return $this->request;
    }

    /**
     * @return Manager
     */
    private function getOrmManager() {
        return Module::getInstance()->getService('ormManager');
    }

    private function initClient() {
        $ormManager = $this->getOrmManager();
        /** @var Client $client */
        $client = $ormManager->createSelectRequest(\VP\Entities\Client::className())
            ->equal('user', $this->userId)
            ->getOne();

        $this->client = $client;

        return $this->client ? true : false;
    }

    private function makeFile($tmpFile) {
        $fileId = \CFile::SaveFile(\CFile::MakeFileArray($tmpFile), '/upload/');
        if ($fileId) {
            return $this->getOrmManager()->createProxy(File::className(), $fileId);
        }
        return null;
    }

    private function savePdfFileToEntity($file) {
        $this->client->lastPdf = $file;
        $this->getOrmManager()->save(array($this->client));
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getWorkPlaces() {
        $workPlaces = array();
        $select = $this->getOrmManager()->createSelectRequest(WorkPlace::className())
            ->addSort('number', 'asc')
            ->equal('company', $this->companyId)
            ->getCollection();

        /** @var WorkPlace $workPlace */
        foreach ($select as $workPlace) {
            if (!$workPlace->id) {
                continue;
            }

            $arWorkPlace['id'] = $workPlace->id;
            $arWorkPlace['generalNumber'] = (int)$workPlace->generalNumber;
            $arWorkPlace['name'] = $workPlace->name;
            $arWorkPlace['workZone'] = $this->getWorkZone($workPlace->id);
            $workPlaces[$workPlace->section->id][] = $arWorkPlace;
        }
        return $workPlaces;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getSections() {
        $sectionLinc = array();
        $numResult['root'] = array();
        $sectionLinc[0] = &$numResult['root'];

        $select = $this->getOrmManager()->createSelectRequest(SectionCompany::className())
            ->addSort('depthLevel', 'asc')
            ->addSort('number', 'asc')
            ->equal('company', $this->companyId)
            ->getCollection();

        /** @var SectionCompany $section */
        foreach ($select as $section) {
            if (!$section->id) {
                continue;
            }
            $arSection = array(
                'id' => $section->id,
                'name' => $section->name,
                'parentId' => $section->parentId,
            );
            $sectionLinc[intval($arSection['parentId'])]['child'][$arSection['id']] = $arSection;
            $sectionLinc[$arSection['id']] = &$sectionLinc[intval($arSection['parentId'])]['child'][$arSection['id']];
        }
        return $numResult['root']['child'];
    }

    private function getSectionItem($name) {
        $html = file_get_contents(__DIR__. "/html/list_events/pdfSection.html");
        $html = strtr($html, array(
            '#SECTION_NAME#' => $name,
        ));
        return $html;
    }

    private function getWorkplaceItem($workplace) {
        $html = '';
        $workZones = $workplace['workZone'];
        if (count($workZones) > 1) {
            $firstZone = array_shift($workZones);
            $tmpHtml = file_get_contents(__DIR__. "/html/list_events/pdfWorkplace.html");
            $html.= strtr($tmpHtml, array(
                '#NAME_WP#' => $workplace['generalNumber'] . ". " . $workplace['name'],
                '#HAZARD#' => $firstZone['hazard'],
                '#ZONE_NAME#' => $firstZone['name'],
                '#RISK_LEVEL#' => $firstZone['riskLevel'],
                '#EVENTS_DOWN_RISK#' => $firstZone['correctionAction']
            ));
            foreach ($workZones as $nextWorkZone) {
                $html.= strtr($tmpHtml, array(
                    '#NAME_WP#' => '-//-',
                    '#HAZARD#' => $nextWorkZone['hazard'],
                    '#ZONE_NAME#' => $nextWorkZone['name'],
                    '#RISK_LEVEL#' => $nextWorkZone['riskLevel'],
                    '#EVENTS_DOWN_RISK#' => $nextWorkZone['correctionAction']
                ));
            }
        } else {
            $firstZone = array_shift($workZones);
            $tmpHtml = file_get_contents(__DIR__. "/html/list_events/pdfWorkplace.html");
            $html.= strtr($tmpHtml, array(
                '#NAME_WP#' => $workplace['generalNumber'] . ". " . $workplace['name'],
                '#HAZARD#' => $firstZone['hazard'],
                '#ZONE_NAME#' => $firstZone['name'],
                '#RISK_LEVEL#' => $firstZone['riskLevel'],
                '#EVENTS_DOWN_RISK#' => $firstZone['correctionAction']
            ));
        }
        return $html;
    }

    private function getMemberItem($arItem) {
        $html = file_get_contents(__DIR__. "/html/list_events/pdfCommission.html");
        $html = strtr($html, array(
            '#MEMBER_POSITION#' => $arItem['POSITION'],
            '#MEMBER_NAME#' => $arItem['NAME']
        ));
        return $html;
    }

    private function initRiskLevel() {
        /** @var RiskLevel $riskLevel */
        $select = $this->getOrmManager()->createSelectRequest(RiskLevel::className())
            ->equal('active', 'Y')
            ->getCollection();

        foreach ($select as $riskLevel) {
            $item['id'] = $riskLevel->id;
            $item['name'] = $riskLevel->name;
            $item['description'] = $riskLevel->description;
            $this->riskLevels[$riskLevel->id] = $item;
        }
    }

    private function getWorkZone($id) {
        $arWorkZones = array();
        $arWorkZone = array();
        /** @var WorkZone $workZone */
        $select = $this->getOrmManager()->createSelectRequest(WorkZone::className())
            ->equal('workPlace', $id)
            ->equal('company', $this->companyId)
            ->getCollection();

        if ($select->count()) {
            foreach ($select as $workZone) {
                if (!$workZone->id) {
                    continue;
                }
                $calcIprSelect = $this->getOrmManager()->createSelectRequest(CalculationIpr::className())
                    ->equal('workZone', $workZone->id)
                    ->getCollection();

                /** @var CalculationIpr $calcIpr */
                foreach ($calcIprSelect as $calcIpr) {
                    $arWorkZone['id'] = $workZone->id;
                    $arWorkZone['name'] = $workZone->name;
                    /** @var Hazard $hazard */
                    $hazard = $this->getOrmManager()->getById(Hazard::className(), $calcIpr->hazard->id);
                    $arWorkZone['hazard'] = $hazard->text;
                    $arWorkZone['riskLevel'] = $this->riskLevels[$calcIpr->riskLevel->id]['name'];
                    $arWorkZone['correctionAction'] = $calcIpr->correctionActivity;
                    $arWorkZones[] = $arWorkZone;
                }
            }
        }
        return $arWorkZones;
    }
}
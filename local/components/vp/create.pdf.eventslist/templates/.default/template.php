<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 * @var string $templateFolder
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDataBase $DB
 */
$this->setFrameMode(true);

$fields = $arResult['fields'];
$errors = $arResult['errors'];
?>
<div class="add-section-form-wrap" style="width: 600px">
    <h3><?=GetMessage('CT_PAGE_TITLE');?></h3>
    <div class="alert">
        <?if (!empty($arResult['errorSave'])):?>
            <div><? echo $arResult['errorSave'];?></div>
        <?endif;?>
        <form name="<?=$arResult['formName'];?>" action="" method="post" class="sectionForm">
            <?=bitrix_sessid_post();?>
            <input type="hidden" name="<?=$arResult['formName']?>[createSection]" value="Y"/>
            <input type="hidden" name="<?=$fields['parentId']['name']?>" maxlength="255" value='<?=$fields['parentId']['value']?>' />
            <input type="hidden" name="<?=$fields['depthLevel']['name']?>" maxlength="255" value='<?=$fields['depthLevel']['value']?>' />
            <input type="hidden" name="<?=$fields['companyId']['name']?>" maxlength="255" value='<?=$fields['companyId']['value']?>' />
            <input type="hidden" name="<?=$fields['number']['name']?>" maxlength="255" value='<?=$fields['number']['value']?>' />
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="<?=$fields['name']['name']?>"><?=$fields['name']['title']?>:</label>
                        <input class="form-control<?=isset($errors['name']) ? " error": "";?>" type="text" placeholder="<?=$fields['name']['placeholder']?>" name="<?=$fields['name']['name']?>" maxlength="255" value='<?=$fields['name']['value']?>' />
                    </div>
                </div>
            <input type="submit" name="createSectionForm[submit]" id="add_section" class="btn btn-success" value="Сохранить" />
        </form>
    </div>
</div>